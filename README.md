# GP User Session Limiter

### Pre-requisites and Installation

* Python v3.8 or later 
* pan-os-python
* pan-python
* PyYAML

* Download and extract this package zip file in a folder at any location.
* Open a command prompt / terminal and cd into the directory where you extracted
 this package (gp-user-session-limiter.zip)

* After installing Python3 (v3.8+) you can install the pre-requisites using pip

``` python3 -m pip install -r requirements.txt ```

## Configuration

Make a copy of config.yml.example and name it config.yml
The configuration format is YAML and the parameters are described below:

```
# This is the config file in YAML format having the Firewalls and GP Gateway 
# names that need to be monitored for multiple logon sesisons.

# WARNING: DO NOT REMOVE OR RENAME THIS FILE. MAKE A COPY AND MODIFY THAT 
# TO MAKE YOUR CONFIG. The config file "config.yml" will be overwritten 
# automatically by the script in case API Keys are not specified and 
# WARNING: All comments / instructions in them will be lost when config.yml
# is overwritten.

# As an example for a stand-alone firewall.

session_limit: 1            # Number of allowed sessions for a user
log_path: './logs'          # Location of log files for this script
daemon_mode: true           # run as a continous process
check_interval: 10          # Number of seconds to wait before re-running (30 is default)
remove_new_sessions: true    # Disconnect newest session instead of oldest.
dry_run: false              # Do not actually log out users.

# This parameter will make the check for user sessions accross all GlobalProtect 
# gateways on the same Firewall / HA Pair. When disabled, the session limit
# Will be applied on individual GP Gateways but same user can connect to other
# GP Gateways defined on the same firewall. 
match_user_across_gateways: true

# This parameter will make the check for user sessions accross multiple firewalls if 
# specified in the config. 
match_across_firewalls: true

# Firewalls to be monitored.
# Note 1: In case of HA firewalls, only one of the firewalls should be defined
#         in the HA section and the peer firewall IP/Hostname should be 
#         provided in the ha_peer_ip field under the firewall entry


firewalls:
  # Definition of a firewall (HA Pair)
  "192.168.1.1":
    api_key: "<API-Key-for-user>"
    gp_gateways:
      - "gp_gateway_name"
      - "gp_gateway_name2"
    # Specify HA peer IP if HA is enabled or leave it empty if HA isn't used.
    ha_peer_ip: "192.168.1.2"       
    # whitelist_users is a list of users that are exempted from sesison limits 
    # and can have unlimited  sessions. The usernames specified here 
    # should have the same format as what appears on the firewall when running
    # the command 'show 
    whitelist_users:               
      - testuser2    
 
  # More firewalls or HA Pair can be defined under the firewalls seciton.
  "192.168.2.1":
    api_key: "<API-Key-for-user>"
    gp_gateways:
      - "gp_gateway_name"
      - "gp_gateway_name2"
    ha_peer_ip: "192.168.2.2"       
    whitelist_users:      # Leave this empty if no whitelist users are needed.

  # Example of a standalone firewall without HA
  "192.168.3.1":
    api_key: "<API-Key-for-user>"
    gp_gateways:
      - "gp_gateway_name"
      - "gp_gateway_name2"
    ha_peer_ip:
    whitelist_users:
```

## Usage

After configuration file is in place, run the script using python3:

``` python3 gp-user-session-tool.py ```

To enable debugging output on console. Use the -debug flag

``` python3 gp-user-session-tool.py -debug ```