#! python3
"""
Script:       gp-user-session-tool.py

Author:       Fahad Yousuf <fyousuf@paloaltonetworks.com>

Description:
Tool to control maximum number of sessions from the same GlobalProtect User.

Requirements:
    - pandevice
    - PyYAML

Interpreter:       Version 3

License:

© 2020 Palo Alto Networks, Inc. All rights reserved.
Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at
https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf

"""

import os
import sys
import time
import getpass
import json
import logging
import logging.handlers as lh
# from datetime import datetime
from xml.etree import ElementTree as et

try:
    from pandevice.firewall import Firewall
except ImportError:
    raise ValueError(
        "pandevice module not available, please install it by running 'python3 -m pip install pandevice'")

try:
    import yaml
except ImportError:
    raise ValueError(
        "PyYAML module not available, please install it by running 'python3 -m pip install PyYAML'")


def load_config(config_file='config.yml'):
    import yaml
    try:
        with open(config_file, "r") as ymlfile:
            cfg = yaml.safe_load(ymlfile)
            return cfg
    except BaseException as e:
        print("Config file 'config.yml' not found or couldn't be opened.")
        exit(1)


def get_config_param(dictpath, param):
    if param not in dictpath.keys():
        return None
    else:
        return dictpath[param]
    return None


# Setup logging
try:
    cfgdict = load_config()
except BaseException as e:
    app_log.error(
        "Unable to load configuration. Please ensure config.yml exists and has no errors")

# Setup logging
if not os.path.exists(cfgdict['log_path']):
    os.mkdir(cfgdict['log_path'])
log_file = os.path.join(cfgdict['log_path'], 'gp-duplicate-user-control.log')
log_rotation_size = 10 * 1024 * 1024  # Size in bytes (This is 10 MB)
log_formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')

file_handler = lh.RotatingFileHandler(
    log_file, maxBytes=log_rotation_size, backupCount=3)
file_handler.setFormatter(log_formatter)
file_handler.setLevel(logging.INFO)

console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
console_handler.setLevel(logging.DEBUG)

app_log = logging.getLogger('root')
if '-debug' in sys.argv:
    app_log.setLevel(logging.DEBUG)
else:
    app_log.setLevel(logging.INFO)
app_log.addHandler(file_handler)
app_log.addHandler(console_handler)


def save_config(cfgdict, config_file='config.yml'):
    with open(config_file, 'w') as outfile:
        yaml.dump(cfgdict, outfile, default_flow_style=False)
    return


def get_credentials(fw_addr):
    app_log.info(
        f"API Key for firewall {fw_addr} is not defined in config.yml")
    print("Please enter the username and password for the user for the API access.")
    print("Configuration file (config.yml) will be updated automatically once the API Key is created.")
    username = input("Enter username: ")
    password = getpass.getpass()
    return username, password


def gen_api_key(fw_addr, username='', password=''):
    if not (len(username) or len(password)):
        username, password = get_credentials(fw_addr)
        try:
            fw_obj = Firewall(fw_addr,
                              api_username=username,
                              api_password=password)
            if fw_obj.api_key:
                app_log.info(
                    f"API Key generated for firewall {fw_addr} for username {username}")
                return fw_obj, fw_obj.api_key
        except BaseException as e:
            app_log.error("Error generating API Key")
            app_log.exception(f"Got exception in gen_api_key: {e.message}")
            exit(1)


def get_gp_gateways(fw_obj):
    """Will return a dictionary with all the gp gateways configured on
    a firewall


    Arguments:
        fw_obj {pandevice.firewall.Firewall} -- Firewall object to get
        GP Gateways from.
    """
    gp_gw_list = list()
    gp_data = fw_obj.op(
        '<show><global-protect-gateway><statistics/></global-protect-gateway></show>', cmd_xml=False, xml=True)
    gp_data = gp_data.decode('utf-8')
    tree = et.fromstring(gp_data)
    gp_gw_list = tree.findall('./result/Gateway/name')
    return [gw_name.text for gw_name in gp_gw_list]


def initialize_fw_objs(cfgdict):
    """[summary]

    Arguments:
        cfgdict {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    fw_dict = cfgdict['firewalls']
    fw_objs = list()

    # Check for API Keys in Config
    config_dirty = 0
    for fw in fw_dict.keys():
        app_log.info(f"Connecting to firewall {fw}")
        try:
            if ('api_key' not in fw_dict[fw].keys()) or (fw_dict[fw]['api_key'] == None) or (fw_dict[fw]['api_key'] == ''):
                fw_obj, api_key = gen_api_key(fw)
                fw_objs.append(fw_obj)
                fw_dict[fw]['api_key'] = api_key
                config_dirty = 1
            else:
                fw_obj = Firewall(
                    fw, api_key=fw_dict[fw]['api_key'], timeout=5)

            if get_config_param(fw_dict[fw], 'ha_peer_ip') != None:
                fw2 = fw_dict[fw]['ha_peer_ip']
                fw_obj_ha = Firewall(fw2, api_key=fw_dict[fw]['api_key'])
                fw_obj.set_ha_peers(fw_obj_ha)

            # Get all GP Gateway names on this FW and append it as an object property
            gp_gws = get_gp_gateways(fw_obj)
            fw_obj.gp_gws = gp_gws
            fw_objs.append(fw_obj)

            if config_dirty:
                cfgdict['firewalls'] = fw_dict
                save_config(cfgdict)
        except BaseException as e:
            app_log.error(
                f"Could not initialize conneciton to {fw}: {e.message}")

    return fw_objs

def simple_username(username: str):
    if '@' in username:
        return username.split('@')[0].lower()
    elif '\\' in username:
        return username.split('\\')[1].lower()
    else:
        return username.lower()

def sessions_by_user(data):
    user_sessions = {}
    for ip, details in data.items():
        for gateway, sessions in details.items():
            for session in sessions:
                user = session['User']
                session['_firewall'] = ip  # Add firewall information to the session details
                if user not in user_sessions:
                    user_sessions[user] = []
                user_sessions[user].append(session)
    return user_sessions

def get_gp_sessions(fw_obj, gp_gateway):
    gp_sessions = dict()
    gp_sessions[gp_gateway] = list()
    try:
        r = fw_obj.op(
            f'<show><global-protect-gateway><current-user><gateway>{gp_gateway}</gateway></current-user></global-protect-gateway></show>', cmd_xml=False, xml=True).decode('utf-8')
        tree = et.fromstring(r)
        if tree.get('status') == 'success':
            for item in tree.findall('./result/entry'):
                if item.find('./username').text != 'pre-logon':
                    log_dict = {
                        'Login Time Epoch': int(item.find('./login-time-utc').text),
                        'Login Time': time.strftime(
                            '%d-%m-%Y %H:%M:%S',
                            time.gmtime(float(item.find('./login-time-utc').text))),
                        'Public IP': item.find('./public-ip').text,
                        'VPN IP': item.find('./virtual-ip').text,
                        'Hostname': item.find('./computer').text,
                        'Domain': item.find('./domain').text,
                        'User': simple_username( item.find('./username').text),
                        'Username': item.find('./username').text,
                        '_gw_name': gp_gateway
                    }
                    if log_dict not in gp_sessions[gp_gateway]:
                        gp_sessions[gp_gateway].append(log_dict)
    except BaseException as e:
        app_log.error(f'Failed to run query on firewall - {fw_obj}')
        app_log.exception(e.message)
        raise
    return gp_sessions


def get_gp_logged_in_users(gp_sessions):
    gpusers = set()
    for entry in gp_sessions:
        gpusers.add(entry['User'])
    return gpusers


def logout_user_session(fw_obj, gp_gw_name, user_session):
    """[summary]

    Arguments:
        fw_obj {[type]} -- [description]
        gp_gw_name {[type]} -- [description]
        user_session {[type]} -- [description]
    """
    if user_session['Domain']:
        cmd_xml_str = f"<request><global-protect-gateway><client-logout>\
            <user>{user_session['Username']}</user>\
            <computer>{user_session['Hostname']}</computer>\
            <domain>{user_session['Domain']}</domain>\
            <gateway>{gp_gw_name}-N</gateway>\
            <reason>force-logout</reason>\
        </client-logout></global-protect-gateway></request>"
    else:
        cmd_xml_str = f"<request><global-protect-gateway><client-logout>\
            <user>{user_session['Username']}</user>\
            <computer>{user_session['Hostname']}</computer>\
            <gateway>{gp_gw_name}-N</gateway>\
            <reason>force-logout</reason>\
        </client-logout ></global-protect-gateway></request>"

    try:
        result = fw_obj.op(cmd_xml_str, cmd_xml=False, xml=True)
    except BaseException:
        raise
    return result.decode('utf-8')


def generate_user_session_index(gp_session_dict: dict) -> list:
    """Returns an nested list of user sessions in the form of a list
    having this structure:     [
        [gp_gw_name {str}, username {str}, session_details {dict}],
        ...
    ]

    Arguments:
        gp_session_dict {dict} - - Dictionary of sessions by gp_gateway as key
    """

    gp_session_index = list()
    for gp_gw_name in gp_session_dict.keys():
        for session in gp_session_dict[gp_gw_name]:
            gp_session_index.append([gp_gw_name, session['User'], session])
    """
    try:         for user in gpusers:            if user not in user_sessions.keys():
                user_sessions[user]= list()
            for session in gp_session_dict[gp_gw_name]:
                if session['User'] == user:
                    user_sessions[user].append(session)
    except BaseException:
        app.error("Unable to check user sessions")
        raise
    """
    return gp_session_index


def execute_logout_limit(user, user_sessions, fw_obj, gp_gw_name=None):
    try:
        app_log.info(
            f'User {user} has more than {cfgdict["session_limit"]} session(s)')
        app_log.debug(user_sessions[user])
        if get_config_param(cfgdict, 'dry_run'):
            app_log.info(
                'Dry Run Flag enabled. No users will be logged out on firewall.')
        else:
            session_list = sorted(
                user_sessions[user], key=lambda k: k['Login Time Epoch'])
            if get_config_param(cfgdict, 'remove_new_sessions'):
                sessions_for_removal = session_list[cfgdict['session_limit']:]
                app_log.info(
                f'Logging out oldest session(s) for user {user}')
            else:
                sessions_for_removal = session_list[:-cfgdict['session_limit']]
                app_log.info(
                f'Logging out newest session(s) for user {user}')
            for s in sessions_for_removal:
                app_log.info(
                    "Session being logged out: {}".format(
                        [s[k] for k in s.keys()]))
                if gp_gw_name != None:
                    logout_result = logout_user_session(
                        fw_obj, gp_gw_name, s)
                else:
                    logout_result = logout_user_session(
                        fw_obj, s['_gw_name'], s)
                app_log.debug(logout_result)
        return False
    except BaseException as e:
        app_log.error(
            f"Error trying to log out sessions for {user}. Message is {e.message}")
        app_log.exception(e.message)
        raise


def logout_user_oldest_sessions(fw_obj, gp_gw_names, gp_session_dict=None):
    if type(gp_gw_names) != list:
        gp_gw_names = [gp_gw_names]

    user_sessions = dict()
    logged_out_cnt = 0
    if gp_session_dict == None:
        gp_session_dict = dict()

    # Build full dict of sessions by gp_gw_name as key
    for gp_gw_name in gp_gw_names:
        if (gp_gw_name not in gp_session_dict):
            gp_sessions_by_gw = get_gp_sessions(fw_obj, gp_gw_name)
            # Merge the two dicts
            gp_session_dict = {**gp_session_dict, **gp_sessions_by_gw}

    gp_user_sessions_index = generate_user_session_index(gp_session_dict)
    whitelist = get_config_param(
        cfgdict['firewalls'][fw_obj.hostname], 'whitelist_users')
    if not ('match_user_across_gateways' in cfgdict.keys() and cfgdict['match_user_across_gateways']):
        for gp_gw_name in gp_gw_names:
            userlist = set([entry[1] for entry in gp_user_sessions_index if
                            (entry[0] == gp_gw_name)])
            user_sessions = dict()
            for user in userlist:
                user_sessions[user] = [entry[2] for entry in gp_user_sessions_index if
                                       (entry[1] == user)]
                if whitelist and (type(whitelist) == list):
                    user_whitelisted = (
                        user in cfgdict['firewalls'][fw_obj.hostname]['whitelist_users'])
                if user_whitelisted:
                    app_log.info(
                        f'Ignoring user "{user}" because this username is whitelisted')
                elif user not in cfgdict['firewalls'][fw_obj.hostname]['whitelist_users']:
                    if len(user_sessions[user]) > cfgdict['session_limit']:
                        execute_logout_limit(
                            user, user_sessions, fw_obj, gp_gw_name=gp_gw_name)
                        logged_out_cnt += 1
    else:
        userlist = set([entry[1] for entry in gp_user_sessions_index])
        user_sessions = dict()
        for user in userlist:
            user_sessions[user] = [entry[2] for entry in gp_user_sessions_index if (
                (entry[1] == user))]
            if whitelist and (type(whitelist) == list):
                user_whitelisted = (
                    user in cfgdict['firewalls'][fw_obj.hostname]['whitelist_users'])
            if whitelist and user_whitelisted:
                app_log.info(
                    f'Ignoring user "{user}" because this username is whitelisted')
            elif len(user_sessions[user]) > cfgdict['session_limit']:
                execute_logout_limit(user, user_sessions, fw_obj)
                logged_out_cnt += 1
    return len(gp_user_sessions_index), logged_out_cnt


def main():
    fw_objs = initialize_fw_objs(cfgdict)
    gp_sessions = dict()
    for fw in fw_objs:
        if fw.ha_peer != None:
            fw.refresh_ha_active()
            fw_active = fw.active()
            cfgdict['firewalls'][fw_active.hostname] = cfgdict['firewalls'][fw.hostname]
            app_log.info(
                f"HA enabled on firewall. Active firewall is {fw_active.hostname}")
        else:
            fw_active = fw
        app_log.info(
            f'Checking for GP sessions on Firewall {fw.hostname}')
        if 'gp_gateways' in cfgdict['firewalls'][fw.hostname]:
            gw_names = [gw_name for gw_name in cfgdict['firewalls']
                        [fw.hostname]['gp_gateways']]
        else:
            gw_names = fw.gp_gws
        if 'match_across_firewalls' in cfgdict.keys() and cfgdict['match_across_firewalls']:
            gp_sessions[fw_active.hostname] = dict()
            for gw in gw_names:
                gp_sessions[fw_active.hostname] = {**gp_sessions[fw_active.hostname], **(get_gp_sessions(fw_active, gw))} 
        total_sessions, logged_out_cnt = logout_user_oldest_sessions(
            fw_active, gw_names)
        app_log.info(
            f'Process completed on firewall {fw_active.hostname} - {total_sessions} sessions processed - {logged_out_cnt} duplicate user sessions found.')
    user_session_dict = sessions_by_user(gp_sessions)
    app_log.debug(f'GP Sessions by User: {json.dumps(user_session_dict, indent=2, sort_keys=True)}')
    if 'match_across_firewalls' in cfgdict.keys() and cfgdict['match_across_firewalls']:
        # Find users with sessions on multiple firewalls
        for user in user_session_dict.keys():
            if len(user_session_dict[user]) > 1:
                app_log.info(f'User {user} has sessions on multiple firewalls')
                user_sessions = user_session_dict[user]
                if 'remove_new_sessions' in cfgdict.keys() and cfgdict['remove_new_sessions']:
                    # Select the newer session
                    logout_target_session = sorted(user_sessions, key=lambda k: k['Login Time Epoch'], reverse=True)[0]
                else:
                    # Select the oldest session
                    logout_target_session = sorted(user_sessions, key=lambda k: k['Login Time Epoch'])[0]
                # Get fw object related to _firewall
                fw_obj = [fw for fw in fw_objs if fw.hostname == logout_target_session['_firewall']][0]
                if not ('dry_run' in cfgdict.keys() and cfgdict['dry_run']):
                    app_log.info(
                        "Session being logged out: {}".format(
                            [logout_target_session[k] for k in logout_target_session.keys()]))
                    logout_user_session(fw_obj, logout_target_session['_gw_name'], logout_target_session)
                    user_session_dict[user].remove(logout_target_session)
                    app_log.debug(f'User sessions after logout: {json.dumps(user_session_dict[user], indent=2, sort_keys=True)}')
                else:
                    app_log.info('Dry Run flag enabled. No users will be logged out.')
    return True


if __name__ == '__main__':
    if 'daemon_mode' in cfgdict.keys() and cfgdict['daemon_mode']:
        try:
            while True:
                start_time = time.time()
                main()
                end_time = time.time()
                elapsed = end_time - start_time
                app_log.info(
                    f"Execution took {elapsed} seconds for all firewalls.")
                if 'check_interval' in cfgdict.keys():
                    time.sleep(cfgdict['check_interval'])
                else:
                    # Default re-check time is 30 seconds.
                    time.sleep(30.0)
        except KeyboardInterrupt as kbi:
            app_log.warning("Ctrl+C pressed. Gracefully exiting.")
            exit(0)
    else:
        main()
        exit(0)
